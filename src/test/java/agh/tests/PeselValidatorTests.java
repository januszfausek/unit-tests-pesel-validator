package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import agh.qa.PeselParser;
import agh.qa.PeselValidator;
import agh.qa.utils.StringUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;

import static agh.qa.PeselParser.Parse;

public class PeselValidatorTests {

    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true}, {"44051401359q", false}, {"4405140135900000", false},
                {"44841401359", false}, {"44222901359", false}, {"43222801359", false}, {"43222901359", false},
                {"44411401359", false}, {"44611401369", false}
        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();
        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
        byte[] peselarray = StringUtils.ToByteArray(pesel);
        Pesel pesel2 = new Pesel (peselarray);
        PeselExtractor peselex = new PeselExtractor(pesel2);
        peselex.GetBirthDate();
        peselex.GetSex();

    }

    @Test
    public void whatever () {
        byte[] peselarray = StringUtils.ToByteArray("44051401359");
        Pesel pesel = new Pesel (peselarray);
        try {
            Assert.assertEquals(false, pesel.getDigit(11));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("index nie może być większy niż 11");
        }
     /* try {
            Assert.assertEquals(false, pesel.getDigit(-1));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("index nie może być mniejszy niż zero");
        }
      */
    }
}

   /*
    @DataProvider
    public Object[][] peselTestDataProvider2() {
        return new Object[][]{

        };
    }

    @Test(dataProvider = "peselTestDataProvider2")
    public void TestPesel2(String pesel, boolean shouldBeValid){
        byte[] pesel2;
        PeselExtractor peselex = new PeselExtractor(new Pesel (pesel2));
    }


        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
}
*/